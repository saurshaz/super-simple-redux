import React from 'react';
import { bindActionCreators } from 'redux';
import { connect, useDispatch, useSelector } from 'react-redux';
import { incrementCount, decrementCount } from './actions/counter';
import './App.css';

export const FunctionalApp = ({}) => {
    const {counter} = useSelector((state) => {
        const {counter} = state;
        return {counter}
    });
    const dispatch = useDispatch();
    
    return (
        <div>
        <div>{counter.count}</div>
        <button onClick={() => {dispatch(incrementCount())}} className="button-primary"> + </button>
        <button onClick={() => {dispatch(decrementCount())} } className="button-primary"> - </button>
      </div>
    )
}

// state binding
// const mapStateToProps = (state) => {
//     const { counter } = state;
//     return {
//         counter,
//     };
// };
// const {counter} = useSelector(mapStateToProps);


// // dispatch-binding
// const mapDispatchToProps = (dispatch) => {
    //   return {
        //     actions: {
//       counter: bindActionCreators(counter, dispatch),
//       // incrementCount: () => {
//       //   console.log('incrementing ...')
//       // }
//     },
//   }
// }

// const AppRedux = connect(
//  mapStateToProps,
//  mapDispatchToProps
// )(App);

// export default AppRedux;
