import React from 'react';
import { bindActionCreators } from 'redux';
import { connect, useDispatch, useSelector } from 'react-redux';
import { incrementCount, decrementCount } from '../actions/counter';
import '../App.css';

export const AppA = ({}) => {
    const {counter} = useSelector((state) => {
        const {counter} = state;
        return {counter}
    });
    const dispatch = useDispatch();
    
    return (
        <div>
            <p>Part A</p>
            <div>{counter.count}</div>
      </div>
    )
}
