import React from 'react';
import { bindActionCreators } from 'redux';
import { connect, useDispatch, useSelector } from 'react-redux';
import { incrementCount, decrementCount } from '../actions/counter';
import '../App.css';

export const AppB = ({}) => {
    const {counter} = useSelector((state) => {
        const {counter} = state;
        return {counter}
    });
    const dispatch = useDispatch();
    
    return (
        <div>
            <p>Part B</p>
            <div>{counter.count}</div>
            <button onClick={() => {dispatch(incrementCount())}} className="button-primary"> + </button>
            <button onClick={() => {dispatch(decrementCount())} } className="button-primary"> - </button>
      </div>
    )
}
