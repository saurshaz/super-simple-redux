export const counter = (state = {count: 15}, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return Object.assign({}, state, {
        count: state.count + action.payload,
      });
    case 'DECREMENT':
      return Object.assign({}, state, {
        count: state.count - action.payload,
      });
    default:
      return state
  }
}